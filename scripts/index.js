const init = () => {
  console.log("loaded");
  const API_KEY = window.GLOBALS ? window.GLOBALS.THE_CAT_API_KEY : null;
  const animatedEl = document.querySelector(".main");
  const titleEl = document.querySelector(".title");
  const descriptionEl = document.querySelector(".description");
  const reloadEl = document.querySelector('.reload');
  console.log(API_KEY);
  document.body.classList.add("loading");
  document.body.classList.remove("no-js");
  console.log(animatedEl);
  const delay = value =>
    new Promise(resolve =>
      setTimeout(() => {
        resolve(value);
      }, 2000)
    );
  const createImage = img => {
    const image = document.createElement("img");
    image.src = img.url;
    image.classList.add("img");
    image.width = img.width;
    // image.height = img.height;
    animatedEl.appendChild(image);
    return img;
  };
  const updateText = img => {
    const breed = img.breeds[0];
    titleEl.textContent = (breed && breed.name) || "A cat";
    descriptionEl.textContent = (breed && breed.description) || "";
    return img;
  };
  const afterAsync = ({ error = null } = {}) => {
    // Once async work is done, restart animation and swap classes on the `body`
    // to clean up from loading state and show dynamic content.
    animatedEl.style.animationPlayState = "running";
    document.body.classList.remove("loading");
    document.body.classList.add("loaded");
    if (error) {
      const errorContentEl = document.createElement("p");
      errorContentEl.textContent = `We encountered a problem: "${error.message}"`;
      animatedEl.appendChild(errorContentEl);
    }
  };
  const fetchData = async () => {
    const image = await fetch(
      "https://api.thecatapi.com/v1/images/search?size=small",
      {
        headers: {
          "x-api-key": API_KEY
        }
      }
    )
      .then(res => res.json())
      .then(delay)
      .then(data => data[0])
      .then(img => {
        console.log(img);
        return img;
      })
      .then(createImage)
      .then(updateText);
    console.log(image);
  };
  const animationStartListener = event => {
    console.log(event);
    // Listen for the `fade_in` event on the main animated element (content container)
    if (event.animationName === "fade_in") {
      console.log(
        `animation play state: ${event.target.style.animationPlayState}`
      );
      // Pause the fade-in animation as soon as it starts
      animatedEl.style.animationPlayState = "paused";
      // Remove the event listener (could use "once", if supported)
      animatedEl.removeEventListener("animationstart", animationStartListener);
      // Now do API or other async work. A new animation will be set up once this work ends (succeeds or fails)
      fetchData()
        .then(() => {
          afterAsync();
          console.log("complete");
        })
        .catch(error => {
          afterAsync({ error });
          console.error(error);
        });
    }
  };
  animatedEl.addEventListener("animationstart", animationStartListener);
  reloadEl.addEventListener('click', ()=>window.location.reload());
  // testEl.addEventListener('animationiteration', handleEvent, false);
  // testEl.addEventListener('animationend', handleEvent, false);
};

window.addEventListener("DOMContentLoaded", init, false);
