module.exports = function(eleventyConfig) {
  eleventyConfig.setBrowserSyncConfig({
    notify: true
  });
  eleventyConfig.addWatchTarget("./_site/css/");
  eleventyConfig.addWatchTarget("./_site/scripts/");
  eleventyConfig.addPassthroughCopy("css");
  eleventyConfig.addPassthroughCopy("scripts");
  // console.log(process.env);
  return {
    passthroughFileCopy: true,
    'the_cat_api_key': process.env.THE_CAT_API_KEY
  };
};
